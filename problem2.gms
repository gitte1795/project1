*Project 1 - Problem 2

Sets
n Set of nodes /n1*n8/
k Set of keys /k1*k30/
;

Alias(n,i,j);

Parameter
T  How many times each must be used at most
q  The number of keys two nodes has to share to communicate directly
Mn Memory of node n in kB
Mk Memory of key k in kB
M  Upperbound
e  Lowebound
;

T = 2;
q = 3;
Mn = 1000;
Mk = 186;
M = 2;
e = 0.1;

Binary Variable
x(n,k)   0-1 variable that indicate if key k is assigned to node n
y(i,j)   0-1 variable that indicate if there is a direct connection
w(i,j,k) 0-1 variable that indicate if node i and j share key k
;

Free Variable
z Objective function value
;

Equations
Obj               Objective function has to maximize the number of direct conections (the if-statemate is to prevent that a node sends to itself and that node j sends back to one that already sent to j)
Memory(n)         Each key occupying 186kB of memory and each node has a memory of 1000kB. This constraint prevent that the limit of memory is exceeded
Keys              Each key must at most be used T = 2 times so we sum over the nodes for all keys and say that a key can at most be in 2 nodes
Connection(i,j,k) If node i and j both include key k then there is a connection and w(ijk)=1 which is used in the constraint below (we don't need the constraint that forces w to be 1 because we maximize)
Directcon(i,j)    If node i and j share three keys then y(ij) = 1 there is a direct connection (y will be 1 since we maximize hence we don't have to have a constraint which forces y to be one)
;

Obj ..                                                       z =e= sum((i,j), y(i,j)$(ord(i) <> ord(j) and ord(i) < ord(j)));
Memory(n) ..                                                 sum(k, x(n,k))*Mk =l= Mn;
Keys(k) ..                                                   sum(n, x(n,k)) =l= T;
Connection(i,j,k)$(ord(i) <> ord(j) and ord(i) < ord(j)) ..  x(i,k) + x(j,k) =g= 2*w(i,j,k);
Directcon(i,j)$(ord(i) <> ord(j) and ord(i) < ord(j)) ..     sum(k, w(i,j,k)) =g= q*y(i,j);



option limrow=1000;
Model problem2 /all/;
Solve problem2 using mip maximizing z;
display z.l, y.l, w.l, x.l;
