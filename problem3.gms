*Project 1 - Problem 3

Sets
v     Set of ships in the company SuperSea /v1*v5/
p     Set of ports SuperSea can visit /Singapore, Incheon, Shanghai, Sydney, Gladstone, Dalian, Osaka, Victoria/
r     Set of routs the company can use /Asia, ChinaPacific/
H1(p) First subset of ports where the company can visit at most one of them /Singapore, Osaka/
H2(p) Second subset of ports where the company can visit at most one of them /Incheon, Victoria/
;

Parameter
F(v) Fixed cost for choosing ship v (in million dollars) /v1 65, v2 60, v3 92, v4 100, v5 110/
G(v) How many days in a year a ship can sail /v1 300, v2 250, v3 350, v4 330, v5 300/
D(p) If company visit port p they have to visit the port at least D(p) times in a year /Singapore 15, Incheon 18, Shanghai 32, Sydney 32, Gladstone 45, Dalian 32, Osaka 15, Victoria 18/
;

Scalar
K How Many ports the company has to service
;

K = 5

Table A(p,r) is 1 if r passes througth port p and 0 otherwise
            Asia   ChinaPacific
Singapore      1              0
Incheon        1              0
Shanghai       1              1
Sydney         0              1
Gladstone      0              1
Dalian         1              1
Osaka          1              0
Victoria       0              1
;


Table T(v,r) for each v it's known how many days needed to complete route r
    Asia  ChinaPacific
v1  14.4          21.2
v2  13.0          20.7
v3  14.4          20.6
v4  13.0          19.2
v5  12.0          20.1
;

Table C(v,r) cost in million dollars due mainly to bunker costs and canal fees
    Asia  ChinaPacific
v1  1.41           1.9
v2   3.0           1.5
v3   0.4           0.8
v4   0.5           0.7
v5   0.7           0.8
;

Binary Variable
u(v) 0-1 variable that indicate if ship v is chosen
y(p) 0-1 variable that indicate if port p is chosen to service
;

Integer Variable
x(v,r) variable that indicates how many times ship v must sail route r
;

Free Variable
z object function value
;

Equations
Obj             Objective function has to minimize the total yearly cost both the cost for each route and the fixed cost for using ship v
Leastports      The company has to service at least K ports
Daysperroute(v) How many routes ship v can sail when we know how long the routes takes and how many dayes ship v and sail in a year
Timesperport(p) If SuperSea choose to visit port p they has to visit the port at least D(p) times in a year
Portpair1       SuperSea can only visit Singapore or Osaka so we sum over the first subset and say that SuperSea only can visit at most one of them
Portpair2       SuperSea can only visit Incheon or Victoria so we sum over the second subset and say that SuperSea only can visit at most one of them
;

Obj ..              z =e= sum((v,r), C(v,r)*x(v,r)) + sum(v, F(v)*u(v));
Leastports ..       sum(p, y(p)) =g= K;
Daysperroute(v) ..  sum(r, x(v,r)*T(v,r)) =l= G(v)*u(v);
Timesperport(p) ..  sum((v,r), A(p,r)*x(v,r)) =g= D(p)*y(p);
Portpair1 ..        sum(H1(p), y(p)) =l= 1;
Portpair2 ..        sum(H2(p), y(p)) =l= 1;


option limrow=1000;
Model problem3 /all/;
Solve problem3 using mip minimizing z;
display z.l, x.l, y.l, u.l;
problem3.OPTCR=0;




