*Project 1 - Problem 1

Sets
i    Set of players /P1*P25/
j    Set of players-roles /GK, CDF, LB, RB, CMF, LW, RW, OMF, CFW, SFW/
k    Set of formulations /442, 352, 4312, 433, 343, 4321/
Q(i) Subset of players - Guality players /P13, P20, P21, P22/
S(i) Subset of players - Strength players /P10, P12, P23/
;


Table c(i,j) Amount of how fit player i is for the player-role j    (table 2)
      GK CDF  LB  RB CMF  LW  RW OMF CFW SFW
P1    10   0   0   0   0   0   0   0   0   0
P2     9   0   0   0   0   0   0   0   0   0
P3   8.5   0   0   0   0   0   0   0   0   0
P4     0   8   6   5   4   2   2   0   0   0
P5     0   9   7   3   2   0   2   0   0   0
P6     0   8   7   7   3   2   2   0   0   0
P7     0   6   8   8   0   6   6   0   0   0
P8     0   4   5   9   0   6   6   0   0   0
P9     0   5   9   4   0   7   2   0   0   0
P10    0   4   2   2   9   2   2   0   0   0
P11    0   3   1   1   8   1   1   4   0   0
P12    0   3   0   2  10   1   1   0   0   0
P13    0   0   0   0   7   0   0  10   6   0
P14    0   0   0   0   4   8   6   5   0   0
P15    0   0   0   0   4   6   9   6   0   0
P16    0   0   0   0   0   7   3   0   0   0
P17    0   0   0   0   3   0   9   0   0   0
P18    0   0   0   0   0   0   0   6   9   6
P19    0   0   0   0   0   0   0   5   8   7
P20    0   0   0   0   0   0   0   4   4  10
P21    0   0   0   0   0   0   0   3   9   9
P22    0   0   0   0   0   0   0   0   8   8
P23    0   3   1   1   8   4   3   5   0   0
P24    0   3   2   4   7   6   5   6   4   0
P25    0   4   2   2   6   7   5   2   2   0
;

Table A(k,j) Amount of player-role j that is needed in formation k  (table 1)
      GK CDF  LB  RB CMF  LW  RW OMF CFW SFW
442    1   2   1   1   2   1   1   0   2   0
352    1   3   0   0   3   1   1   0   1   1
4312   1   2   1   1   3   0   0   1   2   0
433    1   2   1   1   3   0   0   0   1   2
343    1   3   0   0   2   1   1   0   1   2
4321   1   2   1   1   3   0   0   2   1   0
;

Binary Variable
x(i,j)   0-1 variable that indicates if player i is chosen to player-role j
y(k)     0-1 variable that indicates which formulation the coach choose
delta(i) 0-1 variable that indicates if a player is chosen
;

Free Variable
z object function value
;


Equation
Obj             Objective function has to maximize the total fitness player-role
Formulation     The coach has to choose exactly one formulation so we sum over the formulation and say it shall be equal to one
NumPlayInFor(j) The numbers of players must be qual to the needed player-roles in the formulation
Player(i)       A player must play exactly one player-role or non (that means that a player can not play more than one player-role in a formulation)
Quality         The coach has to employ at least one quality player
Strength        The coach has to employ at least one strength player if all quality players are employed
;


Obj ..                z =e= sum((i,j), c(i,j) * x(i,j));
Formulation ..        sum(k, y(k)) =e= 1;
NumPlayInFor(j) ..    sum(i, x(i,j)) =e= sum(k, A(k,j)*y(k));
Player(i) ..          sum(j, x(i,j)) =l= 1;
Quality ..            sum(Q(i), delta(i)) =g= 1;
Strength ..           sum(Q(i), delta(i)) =l= sum(S(i), delta(i)) + 4 -1;


option limrow=1000;
Model problem1 /obj, Formulation, NumPlayInFor, Player, Quality, Strength/;
Solve problem1 using mip maximizing z;
display z.l, x.l, y.l, delta.l;
